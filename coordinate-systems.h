#ifndef _COORDINATE_SYSTEMS_H_
#define _COORDINATE_SYSTEMS_H_

#include <math.h>

namespace coord{
   struct cartesian{
      float x;
      float y;
      float z;
   };
   struct lla{
      float lat;
      float lon;
      float alt;
   };
   struct DCM{
      float matrix[3][3];
   };
   

}
   
coord::cartesian lla2ecef(coord::lla);
coord::cartesian ecef2enu(coord::cartesian, coord::lla);
coord::cartesian ecef2ned(coord::cartesian, coord::lla);
coord::cartesian rotation(coord::cartesian , coord::DCM);
  

#endif // _COORDINATE_SYSTEMS_H_