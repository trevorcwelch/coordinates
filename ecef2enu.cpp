#include "coordinate-systems.h"

// Following the wgs84 geodetic system
coord::cartesian ecef2enu(coord::cartesian pt, coord::lla llaOrigin){
   float cosPhi, sinPhi, cosLambda, sinLambda, t, uEast, wUp, vNorth;

   coord::cartesian origin, diff;
   origin = lla2ecef(llaOrigin);

   diff.x = pt.x - origin.x;
   diff.y = pt.y - origin.y;
   diff.z = pt.z - origin.z;

   cosPhi = cos(llaOrigin.lat * M_PI / 180.0);
   sinPhi = sin(llaOrigin.lat * M_PI / 180.0);
   cosLambda = cos(llaOrigin.lon * M_PI / 180.0);
   sinLambda = sin(llaOrigin.lon * M_PI / 180.0);

   t = (cosLambda * diff.x) +  (sinLambda * diff.y);
   uEast = (-sinLambda * diff.x) + (cosLambda * diff.y);

   wUp = cosPhi * t + sinPhi * diff.z;
   vNorth = -sinPhi * t + cosPhi * diff.z;

   coord::cartesian enu = {.x = uEast, .y = vNorth, .z = wUp};

   return  enu;
}
