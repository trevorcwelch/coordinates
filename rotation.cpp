#include "coordinate-systems.h"

coord::cartesian rotation(coord::cartesian pos, coord::DCM dcm){
   int m, n;
   float a;
   float pv[3] = {pos.x, pos.y, pos.z};
   float pa[3];
   coord::cartesian result;
   m = 3;
   n = 3;

   for (int i = 0; i < m; i++)
   {
      a = 0;
      for (int j = 0; j < n; j++)
      {
         a = a + dcm.matrix[i][j] * pv[j];
      }
      pa[i] = a;
   }
   
   result = {.x = pa[0], .y = pa[1], .z = pa[2]};
   return result;
}
