#include "coordinate-systems.h"

// Following the wgs84 geodetic system
coord::cartesian ecef2ned(coord::cartesian spot, coord::lla llaOrigin){
   coord::cartesian localOrigin, offset, intermediatePos, ned;
   intermediatePos = ecef2enu(spot, llaOrigin);
   ned.x = intermediatePos.y;
   ned.y = intermediatePos.x;
   ned.z = -intermediatePos.z;

   return ned;
}
