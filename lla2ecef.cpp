// From MATLAB lla2ecef.m
#include "coordinate-systems.h"
#include <stdio.h>

struct coord::cartesian lla2ecef(struct coord::lla coordinate){
   float rho, z, x, y, f, R, lat, lon, alt, sinphi, cosphi, e2, N;
   struct coord::cartesian result;

   f = 1.0 / 298.257223563; // flattening factor
   R = 6378137.0; // [m]

   lat = coordinate.lat;
   lon = coordinate.lon;
   alt = coordinate.alt;

   // Determine radial distance from polar axis (rho) and isgned distance from the equator in the spheroid-centric
   // ECEF cylindrical coordinate system
   // Done in degrees
   sinphi = sin(lat * M_PI / 180.0);
   cosphi = cos(lat * M_PI / 180.0);

   e2 = f * (2.0 - f); 
   N = R / sqrt(1 - e2 * pow(sinphi,2));
   rho = (N + alt) * cosphi;

   x = rho * cos(lon * M_PI / 180.0);
   y = rho * sin(lon * M_PI / 180.0);
   z = (N * (1 - e2) + alt) * sinphi;

   result = {.x = x, .y = y, .z= z};

   return result;
}
